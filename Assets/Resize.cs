﻿using UnityEngine;
using System.Collections;

public class Resize : MonoBehaviour {
    private static Resize instance;
    public static Resize Instance
    {
        get
        {
            return instance;
        }
        set
        {
            instance = value;
        }
    }
	// Use this for initialization
	void Awake () {
        instance = this;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
