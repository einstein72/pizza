﻿using UnityEngine;
using System.Collections;

public class Next : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayAnimation()
    {
        gameObject.GetComponent<Animator>().SetTrigger("Pressed");
    }
    public void NextButton()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        Pizza.Instance.NextStage();
    }
}
