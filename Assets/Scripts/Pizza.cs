﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TouchScript.Gestures;
using TouchScript.Gestures.Base;
using System.Collections.Generic;
using System;
using LitJson;
using System.Text;


public enum Dough
{
    Pan,
    thin
}

public enum Types
{
    sauce, cheese,Onion,Olive,Tomatos,Mashroum
}

public enum Stage
{
    Start,Resize,Dough,Stage1,Stage2,Final
}

[Serializable]
public struct PizzaType
{
    public Sprite img;
    public Dough doughType;

}

public class Pizza : MonoBehaviour {
	private static Pizza instance;

    List<Types> rightHalf;
    List<Types> leftHalf;
    List<Types> bothHalfs;
	public static Pizza Instance {
		get {
			return instance;
		}
		set {
			instance = value;
		}
	}

    public Stage stage;
    #region Private variables

    private Transform cachedTransform;
    private List<ITransformGesture> gestures = new List<ITransformGesture>();

    [SerializeField]
    List<PizzaType> pizzaTypes;
    [SerializeField]
    GameObject sPizza;

    [SerializeField]
    GameObject mPizza;

    [SerializeField]
    GameObject lPizza;

    
    [SerializeField]
    GameObject startPanel;
    [SerializeField]
    GameObject sauceImage;
    [SerializeField]
    GameObject resizeContainer;
    [SerializeField]
    Text doughText;
    [SerializeField]
    GameObject stage0Panel;
    [SerializeField]
    GameObject stage1Panel;
    [SerializeField]
    GameObject stage2Panel;
    [SerializeField]
    GameObject stageFinalPanel;

    [SerializeField]
    GameObject sPizzaScale;
    [SerializeField]
    GameObject mPizzaScale;
    [SerializeField]
    GameObject lPizzaScale;

    [SerializeField]
    GameObject arrows;

    [SerializeField]
    GameObject rightHalfAnimation;

    [SerializeField]
    GameObject leftHalfAnimation;


    private TransformGesture transformGestureScript;
    private ReleaseGesture releaseGestureScript;
    private FlickGesture flickGestureScript;

    PizzaType pizzaType;

    Image mImage;
    #endregion

	public bool isItemDragged;
    public HalfPizza halfPizzaSelected;

    #region Unity methods

    private void Awake()
    {
		instance = this;
        rightHalf = new List<Types>();
        leftHalf = new List<Types>();
        bothHalfs = new List<Types>();
        cachedTransform = transform;
        transformGestureScript = gameObject.GetComponent<TransformGesture>();
        releaseGestureScript = Resize.Instance.gameObject.GetComponent<ReleaseGesture>();
        flickGestureScript = gameObject.GetComponent<FlickGesture>();
        mImage = gameObject.GetComponent<Image>();
        pizzaType = pizzaTypes[0];
        mImage.sprite = pizzaTypes[0].img;
    }

    private void OnEnable()
    {
        var g = Resize.Instance.gameObject.GetComponents<Gesture>();
        for (var i = 0; i < g.Length; i++)
        {
            var transformGesture = g[i] as ITransformGesture;
            if (transformGesture == null) continue;

            gestures.Add(transformGesture);
            transformGesture.Transformed += transformHandler;
        }
        if (releaseGestureScript != null)
        {
            releaseGestureScript.Released += ReleaseHandler;
        }
        if (flickGestureScript != null)
        {
            flickGestureScript.Flicked += FlickHandler;
        }
    }

    private void OnDisable()
    {
        for (var i = 0; i < gestures.Count; i++)
        {
            var transformGesture = gestures[i];
            transformGesture.Transformed -= transformHandler;
        }
        gestures.Clear();
        if (releaseGestureScript != null)
        {
            releaseGestureScript.Released += ReleaseHandler;
        }
        if (flickGestureScript != null)
        {
            flickGestureScript.Flicked -= FlickHandler;
        }
    }

    #endregion

    public void AddToTheRightHalf(Types type)
    {
        rightHalf.Add(type);
        RunAnimationRight(type);
    }
    public void AddToTheLeftHalf(Types type)
    {
        leftHalf.Add(type);
        RunAnimationLeft(type);       
    }
    public void AddToAllPizza(Types type)
    {
        bothHalfs.Add(type);
        RunAnimationRight(type);
        RunAnimationLeft(type);
    }

    private void RunAnimationRight(Types type)
    {
        Transform temp;
        switch (type)
        {
            case Types.sauce:
                break;
            case Types.cheese:
                temp = rightHalfAnimation.transform.FindChild("Cheese");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Onion:
                temp = rightHalfAnimation.transform.FindChild("Onion");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Olive:
                temp = rightHalfAnimation.transform.FindChild("Olives");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Tomatos:
                temp = rightHalfAnimation.transform.FindChild("Tomatos");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Mashroum:
                temp = rightHalfAnimation.transform.FindChild("Mash");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    private void RunAnimationLeft(Types type)
    {
        Transform temp;
        switch (type)
        {
            case Types.sauce:
                break;
            case Types.cheese:
                temp = leftHalfAnimation.transform.FindChild("Cheese");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Onion:
                temp = leftHalfAnimation.transform.FindChild("Onion");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Olive:
                temp = leftHalfAnimation.transform.FindChild("Olives");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Tomatos:
                temp = leftHalfAnimation.transform.FindChild("Tomatos");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            case Types.Mashroum:
                temp = leftHalfAnimation.transform.FindChild("Mash");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        temp.GetChild(0).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    case quan.firstShot:
                        temp.GetChild(1).gameObject.SetActive(true);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.Full;
                        break;
                    case quan.Full:
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void DisableAnimationRight(Types type)
    {
        Transform temp;
        switch (type)
        {
            case Types.sauce:
                break;
            case Types.cheese:
                temp = rightHalfAnimation.transform.FindChild("Cheese");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Onion:
                temp = rightHalfAnimation.transform.FindChild("Onion");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Olive:
                temp = rightHalfAnimation.transform.FindChild("Olives");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Tomatos:
                temp = rightHalfAnimation.transform.FindChild("Tomatos");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Mashroum:
                temp = rightHalfAnimation.transform.FindChild("Mash");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    private void DisableAnimationLeft(Types type)
    {
        Transform temp;
        switch (type)
        {
            case Types.sauce:
                break;
            case Types.cheese:
                temp = leftHalfAnimation.transform.FindChild("Cheese");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Onion:
                temp = leftHalfAnimation.transform.FindChild("Onion");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Olive:
                temp = leftHalfAnimation.transform.FindChild("Olives");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Tomatos:
                temp = leftHalfAnimation.transform.FindChild("Tomatos");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            case Types.Mashroum:
                temp = leftHalfAnimation.transform.FindChild("Mash");
                switch (temp.gameObject.GetComponent<Quantity>().quantity)
                {
                    case quan.empty:
                        break;
                    case quan.firstShot:
                        temp.GetChild(0).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.empty;
                        break;
                    case quan.Full:
                        temp.GetChild(1).gameObject.SetActive(false);
                        temp.gameObject.GetComponent<Quantity>().quantity = quan.firstShot;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    public List<string> GetTheRightHalf()
    {
        return rightHalf.ConvertAll<string>(x => x.ToString());
    }
    public List<string> GetTheLeftHalf()
    {
        return leftHalf.ConvertAll<string>(x => x.ToString());

    }
    public List<string> GetAllPizza()
    {
        return bothHalfs.ConvertAll<string>(x => x.ToString());
    }

    public void RemoveFromTheRightHalf(int index)
    {
        if (rightHalf.FindAll(x => x == rightHalf[index]).Count <= 2)
        {
            DisableAnimationRight(rightHalf[index]);
        }
        rightHalf.RemoveAt(index);
    }
    public void RemoveFromTheLeftHalf(int index)
    {
        if (leftHalf.FindAll(x => x == leftHalf[index]).Count <= 2)
        {
            DisableAnimationLeft(leftHalf[index]);
        }
        leftHalf.RemoveAt(index);

    }
    public void RemoveFromAllPizza(int index)
    {
        if (bothHalfs.FindAll(x => x == bothHalfs[index]).Count <= 2)
        {
            DisableAnimationLeft(bothHalfs[index]);
            DisableAnimationRight(bothHalfs[index]);
        }
        bothHalfs.RemoveAt(index);
    }

    public void NextStage()
    {
        if (stage == Stage.Start)
        {
            stage = Stage.Resize;
            startPanel.SetActive(false);
            stage2Panel.SetActive(false);
            stage1Panel.SetActive(false);
            stage0Panel.SetActive(false);
            stageFinalPanel.SetActive(false);
            arrows.SetActive(true);
            resizeContainer.SetActive(true);
        }
        else if(stage == Stage.Resize)
        {
            stage = Stage.Dough;
            stage0Panel.SetActive(true);
            for (var i = 0; i < gestures.Count; i++)
            {
                var transformGesture = gestures[i];
                transformGesture.Transformed -= transformHandler;
            }
            gestures.Clear();
            sPizzaScale.SetActive(false);
            mPizzaScale.SetActive(false);
            lPizzaScale.SetActive(false);
            arrows.SetActive(false);
        }
        else if(stage == Stage.Dough)
        {
            stage = Stage.Stage1;
            stage0Panel.SetActive(false);
            stage1Panel.SetActive(true);
            flickGestureScript.Flicked -= FlickHandler;
            sauceImage.SetActive(true);
        }
        else if(stage == Stage.Stage1)
        {
            stage = Stage.Stage2;
            stage1Panel.SetActive(false);
            stage2Panel.SetActive(true);
        }
        else if (stage == Stage.Stage2)
        {
            stage = Stage.Final;
            stage2Panel.SetActive(false);
            stageFinalPanel.SetActive(true);
        }
    }

    public void NewPizza()
    {
        Application.LoadLevel(1);
    }

    public void Order()
    {
        StringBuilder sb = new StringBuilder();
        JsonWriter writer = new JsonWriter(sb);
        writer.WriteObjectStart();
        writer.WritePropertyName("RightHalf");
        writer.WriteArrayStart();
        for (int i = 0; i < rightHalf.Count; i++)
        {
            writer.Write(rightHalf[i].ToString());
        }
        writer.WriteArrayEnd();
        writer.WritePropertyName("LeftHalf");
        writer.WriteArrayStart();
        for (int i = 0; i < leftHalf.Count; i++)
        {
            writer.Write(leftHalf[i].ToString());
        }
        writer.WriteArrayEnd();
        writer.WritePropertyName("AllPizza");
        writer.WriteArrayStart();
        for (int i = 0; i < bothHalfs.Count; i++)
        {
            writer.Write(bothHalfs[i].ToString());
        }
        writer.WriteArrayEnd();
        writer.WriteObjectEnd();
        print(sb.ToString());
    }

    #region Event handlers

    private void transformHandler(object sender, EventArgs e)
    {
        var gesture = sender as TransformGesture;
        if (!Mathf.Approximately(gesture.DeltaScale, 1f)) cachedTransform.localScale *= gesture.DeltaScale;
        if (!Mathf.Approximately(gesture.DeltaRotation, 0f)) cachedTransform.rotation =
            Quaternion.AngleAxis(gesture.DeltaRotation, Vector3.forward) * cachedTransform.rotation;
    }

    private void ReleaseHandler(object sender, EventArgs e)
    {
        if(transform.localScale.magnitude < sPizza.transform.localScale.magnitude)
        {
            transform.localScale = sPizza.transform.localScale - new Vector3(0.1f,0.1f,0.1f);
        }
        else if((transform.localScale.magnitude > (sPizza.transform.localScale.magnitude))
            && (transform.localScale.magnitude < mPizza.transform.localScale.magnitude))
        {
            transform.localScale = mPizza.transform.localScale - new Vector3(0.15f, 0.15f, 0.15f);
        }
        else if ((transform.localScale.magnitude > (mPizza.transform.localScale.magnitude))
           && (transform.localScale.magnitude < lPizza.transform.localScale.magnitude))
        {
            transform.localScale = lPizza.transform.localScale - new Vector3(0.25f, 0.25f, 0.25f);
        }
        else if((transform.localScale.magnitude > (lPizza.transform.localScale.magnitude)))
        {
            transform.localScale = lPizza.transform.localScale - new Vector3(0.25f, 0.25f, 0.25f);
        }
    }

    private void FlickHandler(object sender, EventArgs e)
    {
        gameObject.GetComponent<Animator>().SetTrigger("Flick");
        
    }

    public void ChangeDoughImage()
    {
        if (pizzaTypes.Count - 1 == (int)pizzaType.doughType)
        {
            pizzaType = pizzaTypes[0];
            mImage.sprite = pizzaTypes[0].img;
            doughText.text = "Stuffed crust";
        }
        else
        {
            pizzaType = pizzaTypes[((int)pizzaType.doughType) + 1];
            mImage.sprite = pizzaTypes[((int)pizzaType.doughType)].img;
            doughText.text = "Thin";
        }
    }

    private void EditPizzaFreedomLevel(bool translation,bool rotation,bool scale)
    {
        if (transformGestureScript != null)
        {
            transformGestureScript.Type = (translation ? TransformGestureBase.TransformType.Translation : 0x0)
                | (rotation ? TransformGestureBase.TransformType.Rotation : 0x0)
                | (scale ? TransformGestureBase.TransformType.Scaling : 0x0);
        }
    }


    #endregion
}
