﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Container : MonoBehaviour {
    VerticalLayoutGroup loGroup;
    [SerializeField]
    GameObject cellPrefab;
    RectTransform m_Rect;
    [SerializeField]
    float minCellWidth;
    [SerializeField]
    float minCellHeight;
    PizzaPart part;

    [SerializeField]
    GameObject eraser;
	// Use this for initialization
	void Awake () {
        loGroup = gameObject.GetComponent<VerticalLayoutGroup>();
        m_Rect = gameObject.GetComponent<RectTransform>();

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void FillTheList(List<string> list,PizzaPart part)
    {
        if (list != null)
        {
            for (int i = 0; i < list.Count; i++)
            {
                AddCell(list[i], i);                
            }

            this.part = part;
        }
    }

    void AddCell(string text,int index)
    {
        GameObject temp = Instantiate(cellPrefab);
        temp.transform.SetParent(transform);
        temp.transform.localScale = Vector3.one;
        temp.transform.GetChild(0).GetComponent<Text>().text = text;
        ContentIndex containerIndex = temp.GetComponent<ContentIndex>();
        containerIndex.index = index;
        containerIndex.container = this;
    }

    public void RemoveCell(int index)
    {
        switch (part)
        {
            case PizzaPart.Right:
                Pizza.Instance.RemoveFromTheRightHalf(index);
                DestroyAllChildren();
                FillTheList(Pizza.Instance.GetTheRightHalf(), part);
                break;
            case PizzaPart.Left:
                Pizza.Instance.RemoveFromTheLeftHalf(index);
                DestroyAllChildren();
                FillTheList(Pizza.Instance.GetTheLeftHalf(), part);
                break;
            case PizzaPart.All:
                Pizza.Instance.RemoveFromAllPizza(index);
                DestroyAllChildren();
                FillTheList(Pizza.Instance.GetAllPizza(), part);
                break;
            default:
                break;
        }
    }

    void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }

    public void Done()
    {
        DestroyAllChildren();
        eraser.SetActive(false);
    }
}
