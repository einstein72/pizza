﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {
    [SerializeField]
    float angle;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        gameObject.transform.Rotate(Vector3.forward, angle);
	}
}
