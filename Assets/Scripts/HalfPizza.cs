﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TouchScript.Gestures;
using TouchScript.Gestures.Base;
using System;
using System.Collections.Generic;

public enum PizzaPart
{
    Right,
    Left,
    All
}
public class HalfPizza : MonoBehaviour, IPointerEnterHandler,IPointerExitHandler {
    [SerializeField]
    GameObject pizzaHover;
    [SerializeField]
    GameObject rightPizzaHover;
    [SerializeField]
    GameObject leftPizzaHover;

    public PizzaPart m_Part;

    private LongPressGesture longPressGestureScript;
    [SerializeField]
    Container container;
    [SerializeField]
    GameObject eraser;
	// Use this for initialization
    void Awake()
    {
        longPressGestureScript = gameObject.GetComponent<LongPressGesture>();
    }
	private void OnEnable()
    {
        if(longPressGestureScript != null)
        {
            longPressGestureScript.LongPressed += LongPressHandler;
        }
    }

    private void OnDisable()
    {
        if (longPressGestureScript != null)
        {
            longPressGestureScript.LongPressed -= LongPressHandler;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void LongPressHandler(object sender,EventArgs e)
    {
        List<string> list = null;
        switch (m_Part)
        {
            case PizzaPart.Right:
               list = Pizza.Instance.GetTheRightHalf();
               container.FillTheList(list,PizzaPart.Right);
                break;
            case PizzaPart.Left:
               list = Pizza.Instance.GetTheLeftHalf();
               container.FillTheList(list, PizzaPart.Left);
                break;
            case PizzaPart.All:
               list = Pizza.Instance.GetAllPizza();
               container.FillTheList(list, PizzaPart.All);
                break;
            default:
                break;
        }
        eraser.SetActive(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
		if (Pizza.Instance.isItemDragged) 
        {

            //if (gameObject.tag != "Middle") {
            //    Color co = gameObject.GetComponent<Image> ().color;
            //    gameObject.GetComponent<Image> ().color = new Color (co.r, co.g, co.b, 1);
            //} else {
            //    pizzaHover.SetActive (true);
            //}
            Pizza.Instance.halfPizzaSelected = this;

            switch (m_Part)
            {
                case PizzaPart.Right:
                    rightPizzaHover.SetActive(true);
                    break;
                case PizzaPart.Left:
                    leftPizzaHover.SetActive(true);
                    break;
                case PizzaPart.All:
                    pizzaHover.SetActive(true);
                    break;
                default:
                    break;
            }
		}

    }

    public void OnPointerExit(PointerEventData eventData)
    {
		
            //if (gameObject.tag != "Middle") 
            //{
            //    Color co = gameObject.GetComponent<Image> ().color;
            //    gameObject.GetComponent<Image> ().color = new Color (co.r, co.g, co.b, 0);
            //} 
            //else 
            //{
            //    pizzaHover.SetActive (false);
            //}

            Pizza.Instance.halfPizzaSelected = null;

            switch (m_Part)
            {
                case PizzaPart.Right:
                    rightPizzaHover.SetActive(false);
                    break;
                case PizzaPart.Left:
                    leftPizzaHover.SetActive(false);
                    break;
                case PizzaPart.All:
                    pizzaHover.SetActive(false);
                    break;
                default:
                    break;
            }
		
    }
}
