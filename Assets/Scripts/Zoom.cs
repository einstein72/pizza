﻿using UnityEngine;
using System.Collections;

public class Zoom : MonoBehaviour {
    public float step;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ZoomIn()
    {
        if (gameObject.transform.localScale.magnitude <= 6.7f)
        {
            gameObject.transform.localScale = gameObject.transform.localScale + (Vector3.one * step);
            print(gameObject.transform.localScale.magnitude);
        }
    }

    public void ZoomOut()
    {
        if (gameObject.transform.localScale.magnitude >= 1.3f)
        {
            gameObject.transform.localScale = gameObject.transform.localScale - (Vector3.one * step);
            print(gameObject.transform.localScale.magnitude);
        }

    }
}
