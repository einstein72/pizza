﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using TouchScript.Gestures;
using TouchScript.Gestures.Base;
public class PizzaCircle : MonoBehaviour,IDragHandler,IEndDragHandler ,IBeginDragHandler{
    [SerializeField]
    PinnedTransformGesture pinnedGestureScript;

    [SerializeField]
    GameObject TypePrefab;
	// Use this for initialization
    [SerializeField]
    Types type;

    [SerializeField]
    GameObject canvas;

    GameObject draggedObject;
    [SerializeField]
    GameObject halfPizza;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnEndDrag(PointerEventData eventData)
    {
        Destroy(draggedObject);
       // eventData.pointerEnter.SetActive(false);
		Pizza.Instance.isItemDragged = false;
        if(Pizza.Instance.halfPizzaSelected)
        {
            switch (Pizza.Instance.halfPizzaSelected.m_Part)
            {
                case PizzaPart.Right:
                    Pizza.Instance.AddToTheRightHalf(type);
                    break;
                case PizzaPart.Left:
                    Pizza.Instance.AddToTheLeftHalf(type);
                    break;
                case PizzaPart.All:
                    Pizza.Instance.AddToAllPizza(type);
                    break;
                default:
                    break;
            }
            Pizza.Instance.halfPizzaSelected.SendMessage("OnPointerExit", eventData);
        }
        pinnedGestureScript.Type = PinnedTrasformGestureBase.TransformType.Rotation;

    }

    public void OnDrag(PointerEventData eventData)
    {
        pinnedGestureScript.Type = 0x0;
		Pizza.Instance.isItemDragged = true;
        Vector2 pos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, eventData.position, Camera.main, out pos);
        draggedObject.transform.position = canvas.transform.TransformPoint(new Vector2(pos.x - 20,pos.y));     
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
       draggedObject = Instantiate(TypePrefab) as GameObject;
       draggedObject.transform.SetParent(canvas.transform,false);
    }
}
