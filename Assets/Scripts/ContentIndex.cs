﻿using UnityEngine;
using System.Collections;

public class ContentIndex : MonoBehaviour {
    public int index;
    public Container container;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    public void Remove()
    {
        container.RemoveCell(index);
    }
}
